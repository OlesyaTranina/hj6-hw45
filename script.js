document.querySelector(".logout").addEventListener('click',function(){
	document.querySelector(".preloader").style.display = 'none';
	document.querySelector(".loginInfo").style.display = 'block';
	document.querySelector(".error").style.display = 'none'; 
	document.querySelector(".userProfile").style.display = 'none'; 

	document.forms.login.email.value = "";
	document.forms.login.password.value = "";
	document.querySelector(".avatar").src = "";
	var profileInfo = document.querySelectorAll("span.user");
	for (i = 0; i < profileInfo.length; i++) {
		profileInfo[i].innerHTML = "";
	}
});

document.forms.login.addEventListener( 'submit', function(event){
	event.preventDefault();
  
  var body;

 if  (this.attributes.enctype.nodeValue == "application/json") {
 	  body = JSON.stringify({email : document.forms.login.email.value,
		password : document.forms.login.password.value}); 
  } else if (this.attributes.enctype.nodeValue == "application/x-www-form-urlencoded") {
  		body = 'email=' + encodeURIComponent(document.forms.login.email.value) +
			'&password=' + encodeURIComponent(document.forms.login.password.value);
  } ;

	xhr = new XMLHttpRequest();
	xhr.open("POST", this.action, true)
	xhr.setRequestHeader('Content-Type', this.attributes.enctype.nodeValue)

	xhr.addEventListener('loadstart', function () {
		document.querySelector(".preloader").style.display = 'block';
		document.querySelector(".loginInfo").style.display = 'none';
		document.querySelector(".error").style.display = 'none';              
	});

	xhr.addEventListener('error', function () {
		document.querySelector(".preloader").style.display = 'none';
		var divError = document.querySelector(".error");
		divError.style.display = 'block';
		divError.innerText = this.response;
	});

	xhr.addEventListener('loadend', function () {
		document.querySelector(".preloader").style.display = 'none';                            
	});

	xhr.addEventListener('load', function () {
		if (this.status == 200) {
			document.querySelector(".userProfile").style.display = 'block'; 
			if (this.responseXML === null) {
				var user = JSON.parse(this.responseText);
			} else {
				var user = getUserFromXML(this.responseXML);
			}
			document.querySelector(".avatar").src = user.userpic;
			document.querySelector(".fullname").innerHTML = "<strong>Name: </strong>" + user.name + " " + user.lastname;
			document.querySelector(".country").innerHTML = "<strong>Country: </strong>" + user.country;
			document.querySelector(".hobbies").innerHTML = "<strong>Hobbies: </strong>" + user.hobbies.join(',')
		} else {
			document.querySelector(".loginInfo").style.display = 'block'; 
			var divError = document.querySelector(".error");
			var EInfo = JSON.parse(this.responseText);
			divError.style.display = 'block';
			divError.innerText = EInfo.error.message;
		};

	});

	xhr.send(body);
});

function getUserFromXML(XML){
	var user = {userpic : "",
	name : "",
	lastname : "",
	country : "",
	hobbies : []};

	var findTag = XML.getElementsByTagName('userpic');
	if (findTag.length > 0) {
		user.userpic =  findTag[0].firstChild.nodeValue;
	} ;

	findTag = XML.getElementsByTagName('name');
	if (findTag.length > 0) {
		user.name =  findTag[0].firstChild.nodeValue;
	} ;
	
	findTag = XML.getElementsByTagName('lastname');
	if (findTag.length > 0) {
		user.lastname =  findTag[0].firstChild.nodeValue;
	} ;
	
	findTag = XML.getElementsByTagName('country');
	if (findTag.length > 0) {
		user.country =  findTag[0].firstChild.nodeValue;
	} ;
	
	findTag = XML.getElementsByTagName('hobbies');
	if (findTag.length > 0) {
		for (i = 0; i < findTag[0].children.length; i++) {
			user.hobbies.push(findTag[0].children[i].firstChild.nodeValue);
		}; 
	} ;
	
	return user;
}